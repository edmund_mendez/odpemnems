import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, CanLoad, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable, of, from } from 'rxjs';

import * as fromAuth from '../auth/store';
import { tap, mergeMap, flatMap, filter, take, switchMap } from 'rxjs/operators';

@Injectable()
export class AuthGuard implements CanActivate {
	authStatus$: Observable<boolean>;

	constructor(private store: Store<fromAuth.AuthState>) {
		this.authStatus$ = this.store.select(fromAuth.getAuthenticated);
	}

	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
		const intendedUrl = route.url.map((seg) => seg.path).join('/');
		return this.isAuthenticated(intendedUrl).pipe(
			switchMap((isAuth) => {
				return of(isAuth);
			})
		);
	}

	isAuthenticated(intendedUrl: string): Observable<boolean> {
		return this.authStatus$.pipe(
			tap((_) => this.store.dispatch(new fromAuth.SetPromptUrl(''))),
			tap((authenticated: boolean) => {
				if (!authenticated) {
					this.store.dispatch(new fromAuth.SetPromptUrl(intendedUrl));
				}
			})
			// take(1)
		);
	}
}
