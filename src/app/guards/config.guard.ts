import { CanActivate, CanActivateChild, CanLoad, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/observable';
import { Store } from '@ngrx/store';
import * as fromStore from '../store';
import { tap, map, take, filter, switchMap, catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable()
export class ConfigGuard implements CanActivate, CanActivateChild {
	constructor(private store: Store<fromStore.RootState>) {}
	canActivate(): Observable<boolean> {
		return this.hasConfigLoaded().pipe(switchMap((isLoaded) => of(true)), catchError((_) => of(false)));
	}

	hasConfigLoaded(): Observable<boolean> {
		return this.store.select(fromStore.getConfigLoaded).pipe(
			tap((isLoaded: boolean) => {
				if (!isLoaded) {
					this.store.dispatch(new fromStore.LoadConfigData());
				}
			}),
			filter((isLoaded) => isLoaded), // continue stream only when isloaded becomes true, if it wasn't before
			take(1)
		);
	}
	canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
		// console.log('inside config.canactivatechild: ', route.url.map((x) => x.path).join('/'));
		return this.canActivate();
	}
}
