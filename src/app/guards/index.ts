import { ConfigGuard } from './config.guard';
import { AuthGuard } from './auth.guard';
export const guards: any[] = [ AuthGuard, ConfigGuard ];

export * from './config.guard';
export * from './auth.guard';
