import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import * as fromServices from './services';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import * as fromStore from './store';
import { LoginComponent } from '../auth/components/login/login.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material';
import { MyHttpInterceptor } from './services/my-htt-interceptor';
import { SERVER_ENDPOINT } from '../shared/constants';
import { RouterModule } from '@angular/router';
@NgModule({
	imports: [
		CommonModule,
		HttpClientModule,
		MatDialogModule,
		RouterModule.forChild([]),
		StoreModule.forFeature('authentication', fromStore.reducers),
		EffectsModule.forFeature([ fromStore.AuthEffect ]),
		FormsModule,
		MatFormFieldModule,
		MatInputModule,
		MatButtonModule
	],
	declarations: [ LoginComponent ],
	entryComponents: [ LoginComponent ],
	providers: [
		{
			provide: HTTP_INTERCEPTORS,
			useClass: MyHttpInterceptor,
			multi: true
		},
		fromServices.AuthenticationService,
		fromServices.LoginServiceService,
		{ provide: SERVER_ENDPOINT, useValue: SERVER_ENDPOINT }
	]
})
export class AuthModule {}
