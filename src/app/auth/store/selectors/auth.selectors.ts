import * as fromFeature from '../reducers';
import * as fromIdentity from '../reducers/auth.reducers';
import * as fromActions from '../actions';
import { createSelector } from '@ngrx/store';

export const getIdentityState = createSelector(fromFeature.getAuthState, (state: fromFeature.AuthState) => {
	return state.identity;
});

export const getUser = createSelector(getIdentityState, fromIdentity.getUser);
export const getAuthenticated = createSelector(getIdentityState, fromIdentity.getAuthenticated);
export const getPromptUrl = createSelector(getIdentityState, fromIdentity.getPromptUrl);
export const getIsPrompted = createSelector(getIdentityState, fromIdentity.getIsPrompted);
export const getUsername = createSelector(getIdentityState, (state) => state.user && state.user.username);

// export const isInGroup = groupName => createSelector(getUser, user => user.usernameslist.indexOf(groupName));
