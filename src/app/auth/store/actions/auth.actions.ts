import { Action } from '@ngrx/store';
import { User } from '../reducers/auth.reducers';

export const GET_IDENTITY = '[Auth] Get User Identity';
export const CHALLENGE = '[Auth] Authentication Challenge';

export const AUTHENTICATE = '[Auth] Authenticate with server';
export const AUTHENTICATE_SUCCESS = '[Auth] Authentication Success';
export const AUTHENTICATE_FAIL = '[Auth] Authentication Failed';

export class GetIdentity implements Action {
	readonly type = GET_IDENTITY;
}
export class ChallengeUser implements Action {
	constructor(public payload: boolean) {}
	readonly type = CHALLENGE;
}

export class Authenticate implements Action {
	readonly type = AUTHENTICATE;
	constructor(public payload: { username: string; password: string }) {}
}

export class AuthenticateSuccess implements Action {
	readonly type = AUTHENTICATE_SUCCESS;
	constructor(public payload: User) {}
}

export class AuthenticateFail implements Action {
	readonly type = AUTHENTICATE_FAIL;
	constructor(public payload: { username: string; password: string }) {}
}
///////
export const LOGOUT = '[Auth] Log Out User';
export const LOGOUT_SUCCESS = '[Auth] Log Out Success';
export const LOGOUT_FAIL = '[Auth] Log Out Fail';

export class Logout implements Action {
	readonly type = LOGOUT;
	constructor(public payload: string) {}
}
export class LogoutSuccess implements Action {
	readonly type = LOGOUT_SUCCESS;
	constructor(public payload: string) {}
}
export class LogoutFail implements Action {
	readonly type = LOGOUT_FAIL;
	constructor(public payload: any) {}
}

///////////

export const SET_URL_BEFORE_PROMPT = '[Auth] Set url at challenge/prompt';

export class SetPromptUrl implements Action {
	readonly type = SET_URL_BEFORE_PROMPT;
	constructor(public payload: string) {}
}
///////////

export const REFRESH_PROMPT_URL = '[Auth] load pre-prompt url';

export class RefreshPromptUrl implements Action {
	readonly type = REFRESH_PROMPT_URL;
	constructor(public payload: string) {}
}

export type AuthActions =
	| GetIdentity
	| ChallengeUser
	| Authenticate
	| AuthenticateSuccess
	| AuthenticateFail
	| Logout
	| LogoutSuccess
	| LogoutFail
	| SetPromptUrl
	| RefreshPromptUrl;
