import { Injectable, Inject } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import * as fromServices from '../../services';
import * as fromActions from '../actions/auth.actions';
import * as fromReducer from '../reducers';
import * as fromSelector from '../selectors';

import { User } from '../reducers/auth.reducers';
import { switchMap, map, tap, catchError, withLatestFrom } from 'rxjs/operators';
import { of } from 'rxjs';
import * as fromRoot from '../../../store';

@Injectable()
export class AuthEffect {
	constructor(
		private actions: Actions,
		private authService: fromServices.AuthenticationService,
		private loginService: fromServices.LoginServiceService,
		private rootStore: Store<fromRoot.RootState>,
		private store: Store<fromReducer.AuthState>
	) {}

	@Effect()
	$getIdentity = this.actions.ofType(fromActions.GET_IDENTITY).pipe(
		switchMap((action: fromActions.GetIdentity) => this.authService.getIdenity()),
		map((resp: any) => {
			if (resp && resp.user) {
				return new fromActions.AuthenticateSuccess(resp.user);
			}
			return new fromActions.ChallengeUser(false);
		}),
		catchError((error) => of({ type: 'NONE_GET_IDENTITY' })) // no need to dispatch anything, interceptor cleans up
	);

	@Effect()
	$authenticate = this.actions.ofType(fromActions.AUTHENTICATE).pipe(
		switchMap((action: fromActions.Authenticate) =>
			this.authService
				.authenticate(action.payload.username, action.payload.password)
				.pipe(catchError((_) => of({ type: 'NONE_AUTHENTICATE' })))
		),
		map((resp: any) => {
			if (resp && resp.user) {
				return new fromActions.AuthenticateSuccess(resp.user);
			}
			return new fromActions.ChallengeUser(false);
		})
	);

	@Effect()
	$challenge = this.actions.ofType(fromActions.CHALLENGE).pipe(
		tap((action: fromActions.ChallengeUser) => {
			// console.log(action.payload);
			if (!action.payload) {
				this.loginService.openDialog();
			}
		}),
		map((_) => ({ type: 'NONE' }))
	);

	@Effect()
	logout$ = this.actions
		.ofType(fromActions.LOGOUT)
		.pipe(
			switchMap((action: fromActions.Logout) =>
				this.authService.logout().pipe(map((_) => new fromActions.LogoutSuccess(action.payload)))
			),
			catchError((error) => of(new fromActions.LogoutFail(error)))
		);

	@Effect()
	redirect$ = this.actions
		.ofType(fromActions.LOGOUT_SUCCESS)
		.pipe(map((action: fromActions.LogoutSuccess) => new fromRoot.Go({ path: [ action.payload ] })));

	// @Effect() refresh$ = this.actions.ofType(fromActions.AUTHENTICATE_SUCCESS).pipe(map((_) => new fromRoot.Refresh()));
	@Effect()
	refresh$ = this.actions.ofType(fromActions.AUTHENTICATE_SUCCESS).pipe(
		withLatestFrom(this.store.select(fromSelector.getPromptUrl)),
		map(([ action, url ]) => {
			if (url.length > 0) {
				return new fromActions.RefreshPromptUrl(url);
			} else {
				return { type: 'NONE' };
			}
		})
	);
	@Effect({ dispatch: true })
	doRefresh$ = this.actions.ofType(fromActions.REFRESH_PROMPT_URL).pipe(
		map((action: fromActions.RefreshPromptUrl) => {
			console.log('redirectig to...', action.payload);
			return new fromRoot.Go({ path: [ action.payload ] });
		})
	);
	/* @Effect({ dispatch: false })
	refresh$ = this.actions.ofType(fromActions.AUTHENTICATE_SUCCESS).pipe(
		tap((_) => {
			this.rootStore.dispatch(new fromRoot.LoadConfigData());
		})
	); */
}
