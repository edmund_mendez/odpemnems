import * as fromIdentification from './auth.reducers';
import { ActionReducerMap, createFeatureSelector } from '@ngrx/store';

export interface AuthState {
	identity: fromIdentification.IdentityState;
}
export const reducers: ActionReducerMap<AuthState> = {
	identity: fromIdentification.reducer
};

export const getAuthState = createFeatureSelector<AuthState>('authentication');
