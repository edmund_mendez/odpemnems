import * as fromAction from '../actions/auth.actions';
import { AuthState } from '.';

export interface User {
	username: string;
	cname: string;
	roles: string[];
	usernameslist: string[];
}

export interface IdentityState {
	authenticated: boolean;
	prompt: boolean;
	urlBeforePrompt: string;
	user: User;
}

const initialState: IdentityState = {
	authenticated: false,
	prompt: false,
	urlBeforePrompt: '',
	user: null
};

export function reducer(state: IdentityState = initialState, action: fromAction.AuthActions): IdentityState {
	switch (action.type) {
		case fromAction.GET_IDENTITY: {
			return state;
		}
		case fromAction.CHALLENGE: {
			return { ...state, authenticated: false, prompt: true };
		}
		case fromAction.AUTHENTICATE_SUCCESS: {
			return { ...state, authenticated: true, user: action.payload, prompt: false };
		}
		case fromAction.LOGOUT_SUCCESS: {
			return { ...state, authenticated: false, user: null };
		}
		case fromAction.LOGOUT_FAIL: {
			console.log(action.payload);
			return state;
		}
		case fromAction.SET_URL_BEFORE_PROMPT: {
			return { ...state, urlBeforePrompt: action.payload };
		}
	}

	return state;
}

export const getAuthenticated = (state: IdentityState) => state.authenticated;
export const getUser = (state: IdentityState) => state.user;
export const getIsPrompted = (state: IdentityState) => state.prompt;
export const getPromptUrl = (state: IdentityState) => state.urlBeforePrompt;
