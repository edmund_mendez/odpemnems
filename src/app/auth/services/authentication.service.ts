import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import { SERVER_ENDPOINT } from '../../shared/constants';

@Injectable()
export class AuthenticationService {
	constructor(private http: HttpClient, @Inject(SERVER_ENDPOINT) private serverUrl: string) {}

	authenticate(username: string, password: string): Observable<any> {
		const URL =
			this.serverUrl +
			'/?login' +
			'&username=' +
			username +
			'&password=' +
			password +
			'&redirectto=' +
			this.serverUrl +
			'/user?openpage';

		return this.http.get(URL);
	}

	getIdenity(): Observable<any> {
		const URL = this.serverUrl + '/user?OpenPage';
		return this.http.get(URL);
	}

	logout(): Observable<any> {
		const URL = this.serverUrl + '/?logout' + '&redirectto=' + this.serverUrl + '/user?openpage';
		return this.http.get(URL).pipe(catchError((error) => of({})));
	}
}
