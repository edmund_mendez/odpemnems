import { throwError as observableThrowError, Observable } from 'rxjs';

import { catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';

import { Store } from '@ngrx/store';
import * as fromStore from '../store';

@Injectable()
export class MyHttpInterceptor implements HttpInterceptor {
	constructor(private store: Store<fromStore.AuthState>) {}

	intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		return next.handle(req).pipe(
			catchError((error, caught) => {
				// intercept the respons error and displace it to the console
				if (error.error.text.indexOf('/names.nsf?Login') > 0) {
					// dispath authenticate action
					let isPrompted = false;
					this.store
						.select(fromStore.getIsPrompted)
						.subscribe((prompted) => (isPrompted = prompted))
						.unsubscribe();
					this.store.dispatch(new fromStore.ChallengeUser(isPrompted));
				}
				// return the error to the method that called it
				return observableThrowError(error);
			})
		) as any;
	}
}
