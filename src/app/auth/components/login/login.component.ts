import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
// import * as fromStore from '../../store';
import { Authenticate } from '../../store/actions/auth.actions';
import { AuthState } from '../../store/reducers';
import { MatDialogRef } from '@angular/material';
@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: [ './login.component.css' ]
})
export class LoginComponent implements OnInit {
	credentials = { username: '', password: '' };
	constructor(private store: Store<AuthState>, private dialogRef: MatDialogRef<LoginComponent>) {}

	ngOnInit() {
		this.credentials = { username: 'lotus administrator', password: 'password' };
	}

	doLogin() {
		this.store.dispatch(new Authenticate(this.credentials));
		this.dialogRef.close();
	}
}
