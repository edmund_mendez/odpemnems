import { throwError as observableThrowError, Observable, of } from 'rxjs';
import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { tap, catchError, map } from 'rxjs/operators';
import { SERVER_ENDPOINT } from '@odpemshared/constants';

@Injectable()
export class ConfigDataService {
	constructor(private http: HttpClient, @Inject(SERVER_ENDPOINT) private serverUrl: string) {}

	getConfigData(): Observable<any> {
		const URL = this.serverUrl + '/event.xsp/events';
		return this.http.get(URL).pipe(
			// tap(data => console.dir(data)),
			map((data: any[]) =>
				data.map((x) => {
					x.unid = x['@unid'];
					delete x['@unid'];
					delete x['@entryid'];
					return x;
				})
			),
			catchError((error: any) => observableThrowError(error.json()))
		);
	}
}
