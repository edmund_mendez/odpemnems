import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { AuthModule } from './auth/auth.module';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import * as fromStore from './store';
import { StoreRouterConnectingModule, RouterStateSerializer } from '@ngrx/router-store';
import * as fromContainers from './containers';
import { AppMaterialModule } from './app-material/material.module';
import * as fromServices from './services';
import * as fromGuards from './guards';

const routes: Routes = [
	{
		path: '',
		redirectTo: 'eoc',
		pathMatch: 'full'
	},
	{
		path: 'eoc',
		canActivate: [ fromGuards.AuthGuard ],
		canActivateChild: [ fromGuards.ConfigGuard ],
		component: fromContainers.EochomeComponent,
		// canLoad: [ fromGuards.AuthGuard ],
		// runGuardsAndResolvers: 'always',
		children: [
			{
				path: '',
				redirectTo: 'eventselect',
				pathMatch: 'full'
			},
			{
				path: ':eventId/:roleId/eventselect',
				component: fromContainers.EventSelectorComponent
			},
			{
				path: 'eventselect',
				component: fromContainers.EventSelectorComponent
			},
			{
				path: ':eventId/:roleId/dashboard',
				loadChildren: './dashboard/dashboard.module#DashboardModule'
			},
			{
				path: ':eventId/:roleId/response',
				loadChildren: './response/response.module#ResponseModule'
			}
		]
	},
	{
		path: 'agency',
		canActivate: [ fromGuards.AuthGuard ],
		component: fromContainers.AgencyHomeComponent
	}
];

// const routes: Routes = [ { path: '', component: AppComponent } ];
@NgModule({
	declarations: [ AppComponent, ...fromContainers.containers ],
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		FormsModule,
		ReactiveFormsModule,
		AppMaterialModule,
		StoreModule.forRoot(fromStore.reducers),
		EffectsModule.forRoot(fromStore.effects),
		RouterModule.forRoot(routes),
		StoreRouterConnectingModule,
		AuthModule,
		StoreDevtoolsModule.instrument()
	],
	providers: [
		...fromServices.services,
		...fromGuards.guards,
		{ provide: RouterStateSerializer, useClass: fromStore.CustomSerializer }
	],
	bootstrap: [ AppComponent ]
})
export class AppModule {}
