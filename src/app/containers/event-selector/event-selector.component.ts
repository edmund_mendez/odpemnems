import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromStore from '../../store';
import { Observable } from 'rxjs/observable';
import * as fromModels from '@odpemshared/models';
import { Router } from '@angular/router';
@Component({
	selector: 'app-event-selector',
	templateUrl: './event-selector.component.html',
	styleUrls: [ './event-selector.component.css' ]
})
export class EventSelectorComponent implements OnInit {
	events$: Observable<fromModels.OdpemEvent[]>;
	roles$: Observable<fromModels.UserRole[]>;

	selection = {
		eventId: '',
		roleId: ''
	};

	constructor(private store: Store<fromStore.RootState>, private router: Router) {}

	ngOnInit() {
		// this.store.dispatch(new fromStore.LoadConfigData());
		this.events$ = this.store.select(fromStore.getEvents);
		this.roles$ = this.store.select(fromStore.getRoles);
	}
	keepEventRole() {}
	changeEventRole() {
		// this.store.dispatch(new fromStore.SetEventRole({eventid: this.selection.eventId, roleid: this.selection.roleId}));
		// this.store.dispatch(new fromStore.UnLoadMessages());
		this.router.navigate([ '/eoc/' + this.selection.eventId + '/' + this.selection.roleId + '/dashboard' ]);
	}
}
