import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { Observable } from 'rxjs';

@Component({
	selector: 'agency-home',
	templateUrl: './agency-home.component.html',
	styleUrls: [ './agency-home.component.css' ]
})
export class AgencyHomeComponent {
	isHandset: Observable<BreakpointState> = this.breakpointObserver.observe(Breakpoints.Handset);
	constructor(private breakpointObserver: BreakpointObserver) {}
}
