import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EochomeComponent } from './eochome.component';

describe('EochomeComponent', () => {
  let component: EochomeComponent;
  let fixture: ComponentFixture<EochomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EochomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EochomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
