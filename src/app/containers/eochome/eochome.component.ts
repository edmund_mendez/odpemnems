import { Component, OnInit, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { Store } from '@ngrx/store';
import * as fromAuth from '../../auth/store';
import { Observable } from 'rxjs/observable';
import { tap } from 'rxjs/operators';
import * as fromStore from '../../store';

@Component({
	selector: 'app-eochome',
	templateUrl: './eochome.component.html',
	styleUrls: [ './eochome.component.css' ]
})
export class EochomeComponent implements OnInit, OnDestroy {
	username$: Observable<string>;
	eventId$: Observable<string>;
	eventName$: Observable<string>;
	roleId$: Observable<string>;

	mobileQuery: MediaQueryList;
	_mobileQueryListener;

	constructor(
		private store: Store<fromStore.RootState>,
		private authStore: Store<fromAuth.AuthState>,
		private changeDetectorRef: ChangeDetectorRef,
		private media: MediaMatcher
	) {}

	ngOnInit() {
		this.username$ = this.authStore.select(fromAuth.getUsername);
		this.eventId$ = this.store.select(fromStore.getCurrentEvent);
		this.eventName$ = this.store.select(fromStore.getCurrentEventName);
		this.roleId$ = this.store.select(fromStore.getCurrentUserRole);

		this.mobileQuery = this.media.matchMedia('(max-width: 600px)');
		this._mobileQueryListener = () => this.changeDetectorRef.detectChanges();
		this.mobileQuery.addListener(this._mobileQueryListener);
	}

	ngOnDestroy(): void {
		this.mobileQuery.removeListener(this._mobileQueryListener);
	}
	onLogout() {
		// this.store.dispatch(new fromStore.UnLoadMessages());
		const redirectUrl = '/eoc';
		this.authStore.dispatch(new fromAuth.Logout(redirectUrl));
	}
}
