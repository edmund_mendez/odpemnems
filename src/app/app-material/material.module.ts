import { NgModule } from '@angular/core';
import {
	MatRadioModule,
	MatSelectModule,
	MatSidenavModule,
	MatCheckboxModule,
	MatListModule,
	MatIconModule,
	MatCardModule,
	MatMenuModule,
	MatInputModule,
	MatFormFieldModule,
	MatGridListModule
} from '@angular/material';
import { MatButtonModule } from '@angular/material/button';
import { MatTableModule } from '@angular/material/table';
import { CdkTableModule } from '@angular/cdk/table';
import { MediaMatcher } from '@angular/cdk/layout';

import { MatToolbarModule } from '@angular/material/toolbar';
const matModules = [
	MatToolbarModule,
	MatCheckboxModule,
	MatSidenavModule,
	MatListModule,
	MatIconModule,
	MatMenuModule,
	MatCardModule,
	MatButtonModule,
	MatSelectModule,
	MatInputModule,
	MatFormFieldModule,
	MatRadioModule,
	MatTableModule,
	CdkTableModule,
	MatGridListModule
];

@NgModule({
	imports: [ ...matModules ],
	declarations: [],
	providers: [ MediaMatcher ],
	exports: [ ...matModules ]
})
export class AppMaterialModule {}
