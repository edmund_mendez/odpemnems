export * from './configdata.service';
export * from './messages.service';

import { ConfigDataService } from './configdata.service';
import { MessagesService } from './messages.service';

export const services = [ ConfigDataService, MessagesService ];
