import { throwError as observableThrowError } from 'rxjs';
import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/observable';
import { Message } from '../../shared/models/message.model';
import { SERVER_ENDPOINT } from '../../shared/constants';
import { catchError, tap, map, switchMap } from 'rxjs/operators';

@Injectable()
export class MessagesService {
	constructor(private http: HttpClient, @Inject(SERVER_ENDPOINT) private serverUrl: string) {}

	getMessagebyUnid(unid: string): Observable<any> {
		const URL = this.serverUrl + '/message.xsp/message/' + unid;
		return this.http.get(URL).pipe(catchError((error: any) => observableThrowError(error.json())));
	}

	loadMessages(eventUnid: string): Observable<Message[]> {
		const URL = this.serverUrl + '/message.xsp/messages?keys=' + eventUnid;
		return this.http
			.get(URL)
			.pipe(
				map((resp: any[]) => this.sanitizeMessageCollection(resp)),
				catchError((error: any) => observableThrowError(error.json()))
			);
	}

	updateMessage(payload: Message): Observable<Message> {
		const URL = this.serverUrl + '/message.xsp/message/' + payload.unid;
		const newPayload = payload;

		return this.http
			.put(URL, newPayload, { observe: 'response' })
			.pipe(
				switchMap((_) => this.getMessagebyUnid(payload.unid)),
				map((rawDoc) => this.sanitizeMessage(rawDoc)),
				catchError((error: any) => observableThrowError(error.json()))
			);
	}

	createMessage(payload: Message): Observable<Message> {
		const URL = this.serverUrl + '/message.xsp/message/';
		const newPayload = {
			...payload,
			MessageDateTime:
				payload.MessageDateTime.toLocaleDateString() + ' ' + payload.MessageDateTime.toLocaleTimeString()
		};

		return this.http.post(URL, newPayload, { observe: 'response' }).pipe(
			map((resp) => {
				const urlSplit = resp.headers.get('location').split('/');
				return urlSplit[urlSplit.length - 1];
			}),
			switchMap((unid) => this.getMessagebyUnid(unid)),
			map((rawDoc) => this.sanitizeMessage(rawDoc)),
			catchError((error: any) => observableThrowError(error.json()))
		);
	}

	sanitizeMessageCollection(rawDocs: any[]): Message[] {
		const newColl = rawDocs.map((rawDoc) => {
			rawDoc.unid = rawDoc['@unid'];
			delete rawDoc['@unid'];
			delete rawDoc['@entryid'];
			/*             delete rawDoc['@noteid'];
            delete rawDoc['@position'];
            delete rawDoc['@siblings'];
            delete rawDoc['@indent'];
            delete rawDoc['@form'];
            delete rawDoc['@read']; */
			return rawDoc;
		});
		return newColl;
	}

	sanitizeMessage(rawDoc: any): Message {
		rawDoc.unid = rawDoc['@unid'];
		delete rawDoc['@unid'];
		delete rawDoc['@authors'];
		delete rawDoc['@created'];
		delete rawDoc['@form'];
		delete rawDoc['@modified'];
		delete rawDoc['@noteid'];
		delete rawDoc['$UpdatedBy'];
		return rawDoc;
	}
}
