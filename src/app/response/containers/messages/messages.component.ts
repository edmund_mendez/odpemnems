import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/observable';
import { tap } from 'rxjs/operators';
import * as fromRoot from '@odpemroot/store';
import * as fromStore from '../../store';
import { Message } from '@odpemshared/models/message.model';
import { UserRole } from '@odpemshared/models';

@Component({
	// selector: 'app-messages',
	templateUrl: './messages.component.html',
	styleUrls: [ './messages.component.css' ]
})
export class MessagesComponent implements OnInit {
	displayedColumns = [ 'callerName', 'callerParish', 'incidentDateTime', 'incidentParish' ];
	messages$: Observable<Message[]>;
	currentUserRole$: Observable<UserRole>;
	currentEventId$: Observable<string>;

	constructor(private store: Store<fromStore.EOCState>, private rootStore: Store<fromRoot.RootState>) {}

	ngOnInit() {
		this.currentEventId$ = this.rootStore.select(fromRoot.getCurrentEvent);
		/*this.currentEventId$
			.subscribe((eventUnid) => this.store.dispatch(new fromStore.LoadMessages(eventUnid)))
			.unsubscribe(); */
		this.currentUserRole$ = this.rootStore.select(fromRoot.getCurrentUserRole);
		this.messages$ = this.store.select(fromStore.getMessages);
	}
}
