import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromStore from '../../store';
import * as fromRoot from '@odpemroot/store';
import { Message } from '@odpemshared/models/message.model';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { tap, withLatestFrom } from 'rxjs/operators';
@Component({
	selector: 'app-message',
	templateUrl: './message.component.html',
	styleUrls: [ './message.component.css' ]
})
export class MessageComponent implements OnInit {
	private callerDetails: FormGroup;
	private incidentDetails: FormGroup;
	private messageDetails: FormGroup;
	private message$: Observable<Message>;
	private message: Message;

	constructor(
		private store: Store<fromStore.EOCState>,
		private rootStore: Store<fromRoot.RootState>,
		private fb: FormBuilder
	) {}

	ngOnInit() {
		this.setupForm();

		const eventName$ = this.rootStore.select(fromRoot.getCurrentEventName);

		this.message$ = this.store.select(fromStore.getSelectedMessage);
		const username$ = this.rootStore.select(fromRoot.getCurrentUsername);
		const eventId$ = this.rootStore.select(fromRoot.getCurrentEvent);
		this.message$
			.pipe(withLatestFrom(eventId$, eventName$, username$))
			.subscribe(([ mess, eventId, eventName, username ]) => {
				if (mess) {
					this.message = mess;
					this.messageDetails.patchValue(mess);
					this.callerDetails.patchValue(mess);
					this.incidentDetails.patchValue(mess);
				} else {
					// this is a new document
					this.messageDetails.patchValue({
						MessageEventId: eventId,
						MessageEvent: eventName,
						MessageAuthor: username
					});
				}
			});
		// this.mockMessage();
	}
	getMessageFromForm(): Message {
		const doc: Message = {
			...this.messageDetails.value,
			...this.callerDetails.value,
			...this.incidentDetails.value
		};
		return doc;
	}
	onCloseMessage() {
		this.store.dispatch(new fromStore.NavigateMessages('mainview'));
	}
	onUpdateMessage() {
		const oldMessage = this.message;
		const doc = Object.assign({}, oldMessage, this.getMessageFromForm());
		this.store.dispatch(new fromStore.UpdateMessage(doc));
	}
	createMessage() {
		const doc = this.getMessageFromForm();
		this.store.dispatch(new fromStore.CreateMessage(doc));
		// console.dir(doc);
	}

	setupForm() {
		this.messageDetails = this.fb.group({
			MessageAuthor: [ '', Validators.required ],
			MessageDateTime: [ new Date(), Validators.required ],
			MessageEventId: [ '', Validators.required ],
			MessageEvent: [ '', Validators.required ],
			MessagePrecedence: [ '', Validators.required ],
			MessageSource: [ '', Validators.required ],
			MessageStatus: [ '', Validators.required ]
			// MessageId: [ '' ]
		});

		this.callerDetails = this.fb.group({
			CallerName: [ '', Validators.required ],
			CallerAddress1: [ '', Validators.required ],
			CallerAddress2: [ '', Validators.required ],
			CallerEmail: [ '', Validators.email ],
			CallerParish: [ '', Validators.required ],
			CallerPhone1: [ '', Validators.required ]
		});

		this.incidentDetails = this.fb.group({
			IncidentDateTime: [ new Date(), Validators.required ],
			IncidentAddress1: [ '', Validators.required ],
			IncidentAddress2: [ '', Validators.required ],
			IncidentDescription: [ '', Validators.required ],
			IncidentParish: [ '', Validators.required ],
			IncidentSituations: [ '', Validators.required ],
			DeathCount: [ 0, Validators.required ],
			InjuryCount: [ 0, Validators.required ],
			MissingCount: [ 0, Validators.required ]
		});
		// const message: Observable<Message> = this.store.select(fromStore.getSelectedMessage)
	}

	// =====================

	mockMessage() {
		this.messageDetails.patchValue({
			MessagePrecedence: '1',
			MessageSource: 'Phone',
			MessageStatus: '0'
		});

		this.callerDetails.patchValue({
			CallerName: 'John Brown',
			CallerAddress1: '21 Orange Street',
			CallerAddress2: 'Kingston 20',
			CallerEmail: 'edmundm@gmail.com',
			CallerParish: 'Kingston',
			CallerPhone1: '999-999-9999'
		});

		this.incidentDetails.patchValue({
			IncidentDateTime: new Date(),
			IncidentAddress1: 'Same',
			IncidentAddress2: 'Same',
			IncidentDescription: 'The quick brown fox jumped over the lazy dog!',
			IncidentParish: 'Kingston',
			IncidentSituations: 'Weeping, Moaning',
			DeathCount: 0,
			InjuryCount: 1,
			MissingCount: 10
		});
	}
}
