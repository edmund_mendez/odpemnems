import { MessageComponent } from './message/message.component';
import { MessagesComponent } from './messages/messages.component';
import { ActionsComponent } from './actions/actions.component';

export const containers: any[] = [ MessagesComponent, MessageComponent, ActionsComponent ];

export * from './actions/actions.component';
export * from './messages/messages.component';
export * from './message/message.component';
