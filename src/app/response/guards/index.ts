import { MessagesGuard } from './messages.guard';

export const guards: any[] = [ MessagesGuard ];

export * from './messages.guard';
