import { CanActivate } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/observable';
import { Store } from '@ngrx/store';
import * as fromStore from '../store';
import * as fromRoot from '../../store';

import { tap, map, take, filter, switchMap, catchError, withLatestFrom } from 'rxjs/operators';
import { of } from 'rxjs';
import { isLoweredSymbol } from '@angular/compiler';

@Injectable()
export class MessagesGuard implements CanActivate {
	eventId$: Observable<string>;
	constructor(private store: Store<fromStore.EOCState>, private rootStore: Store<fromRoot.RootState>) {
		this.eventId$ = this.rootStore.select(fromRoot.getCurrentEvent);
	}

	canActivate(): Observable<boolean> {
		return this.messagesLoaded().pipe(switchMap((isLoaded) => of(true)), catchError((_) => of(false)));
	}

	messagesLoaded(): Observable<boolean> {
		return this.store.select(fromStore.getMessageIsLoaded).pipe(
			withLatestFrom(this.eventId$),
			tap(([ isLoaded, eventId ]) => {
				if (!isLoaded) {
					this.store.dispatch(new fromStore.LoadMessages(eventId));
				}
			}),
			filter(([ isLoaded, eventId ]) => isLoaded),
			map(([ isLoaded, eventId ]) => isLoaded),
			take(1)
		);
	}
}
