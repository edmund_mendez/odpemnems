import { createSelector } from '@ngrx/store';
import * as fromFeature from '../reducers';
import * as fromReducer from '../reducers/messages.reducer';
import { Message } from '../../../shared/models/message.model';
import * as fromRoot from '../../../store';

const getMessageState = createSelector(fromFeature.getEOCState, (state) => state.messages);

export const getMessageIsLoaded = createSelector(getMessageState, fromReducer.getMessageIsLoaded);
export const getMessageIsLoading = createSelector(getMessageState, fromReducer.getMessageIsLoading);
export const getMessageEntities = createSelector(getMessageState, fromReducer.getMessageEntities);

export const getMessages = createSelector(getMessageEntities, (entities: { [unid: string]: Message }) => {
	return Object.keys(entities).map((key) => entities[key]);
});

export const getSelectedMessage = createSelector(
	fromRoot.getRootState,
	getMessageEntities,
	(routerState, entities): Message => {
		return routerState.router.state && entities[routerState.router.state.params['unid']];
	}
);
