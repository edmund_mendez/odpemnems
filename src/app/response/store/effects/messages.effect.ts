import { Injectable, Inject } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import * as fromServices from '../../services';
import * as fromActions from '../actions/messages.action';
import { switchMap, map, tap, catchError, withLatestFrom } from 'rxjs/operators';
import { of } from 'rxjs';
import { Message } from '../../../shared/models/message.model';
import * as fromRoot from '../../../store';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/observable';
@Injectable()
export class MessagesEffect {
	constructor(
		private actions$: Actions,
		private messagesService: fromServices.MessagesService,
		private store: Store<fromRoot.RootState>
	) {}

	@Effect()
	$createMessage = this.actions$
		.ofType(fromActions.CREATE_MESSAGE)
		.pipe(
			switchMap((action: fromActions.CreateMessage) => this.messagesService.createMessage(action.payload)),
			map((message: Message) => new fromActions.CreateMessageSuccess(message)),
			catchError((error) => of(new fromActions.CreateMessageFail(error)))
		);

	@Effect()
	$updateMessage = this.actions$.ofType(fromActions.UPDATE_MESSAGE).pipe(
		switchMap((action: fromActions.UpdateMessage) =>
			this.messagesService.updateMessage(action.payload).pipe(
				catchError((error) => {
					// console.log('update error');
					return of(new fromActions.UpdateMessageFail(error));
				})
			)
		),
		map((message: Message) => new fromActions.UpdateMessageSuccess(message))
	);

	@Effect()
	$createSuccess = this.actions$.ofType(fromActions.CREATE_MESSAGE_SUCCESS).pipe(
		map((action: fromActions.CreateMessageSuccess) => action.payload),
		withLatestFrom(this.store.select(getEventId), this.store.select(getRoleId)),
		map(([ message, eventId, role ]) => {
			return new fromRoot.Go({ path: [ '/eoc', eventId, role, 'messages', message.unid ] });
		}),
		catchError((error) => of(new fromActions.CreateMessageFail(error)))
	);

	@Effect()
	$navigate = this.actions$.ofType(fromActions.NAVIGATE_MESSAGES).pipe(
		withLatestFrom(this.store.select(getEventId), this.store.select(getRoleId)),
		map(([ view, eventId, role ]) => {
			// not using view right now but could in the future to
			// determine which url to redirect to
			return new fromRoot.Go({ path: [ '/eoc', eventId, role, 'response', 'messages' ] });
		}),
		catchError((error) => of(new fromActions.CreateMessageFail(error)))
	);

	@Effect()
	$loadMessages = this.actions$
		.ofType(fromActions.LOAD_MESSAGES)
		.pipe(
			switchMap((action: fromActions.LoadMessages) => this.messagesService.loadMessages(action.payload)),
			map((messages: Message[]) => new fromActions.LoadMessagesSuccess(messages)),
			catchError((error) => of(new fromActions.LoadMessagesFail(error)))
		);
}

function getEventId(rootState): Observable<string> {
	return rootState.router && rootState.router.state.params['eventId'];
}

function getRoleId(rootState): Observable<string> {
	return rootState.router && rootState.router.state.params['roleId'];
}
