import { ActionReducerMap, createFeatureSelector } from '@ngrx/store';
import * as fromMessages from './messages.reducer';

export interface EOCState {
	messages: fromMessages.MessagesState;
}

export const reducers: ActionReducerMap<EOCState> = {
	messages: fromMessages.reducer
};

export const getEOCState = createFeatureSelector<EOCState>('eoc');
