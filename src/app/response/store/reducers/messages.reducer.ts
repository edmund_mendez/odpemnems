import { Message } from '../../../shared/models/message.model';
import * as fromActions from '../actions/messages.action';

export interface MessagesState {
	loading: boolean;
	loaded: boolean;
	entities: { [unid: string]: Message };
}

export const initialState: MessagesState = {
	loading: false,
	loaded: false,
	entities: {}
};

export function reducer(state: MessagesState = initialState, action: fromActions.MessageActions): MessagesState {
	switch (action.type) {
		case fromActions.LOAD_MESSAGES: {
			return { ...state, loading: true };
		}
		case fromActions.LOAD_MESSAGES_FAIL: {
			return { ...state, loading: false, loaded: false };
		}
		case fromActions.LOAD_MESSAGES_SUCCESS: {
			const messages: Message[] = action.payload;
			const entities = messages.reduce((acc: { [id: string]: Message }, mess: Message) => {
				/* acc[mess.unid] = mess;
                return acc; */
				return { ...acc, [mess.unid]: mess };
			}, {});
			return { ...state, loaded: true, entities };
		}
		case fromActions.UPDATE_MESSAGE_SUCCESS:
		case fromActions.CREATE_MESSAGE_SUCCESS: {
			const message: Message = action.payload;
			const entities = { ...state.entities, [message.unid]: message };
			return { ...state, entities };
		}
		case fromActions.UNLOAD_MESSAGES: {
			const entities = {};
			return { ...state, loaded: false, entities };
		}
	}
	return state;
}

export const getMessageIsLoading = (state: MessagesState) => state.loading;
export const getMessageIsLoaded = (state: MessagesState) => state.loaded;
export const getMessageEntities = (state: MessagesState) => state.entities;
