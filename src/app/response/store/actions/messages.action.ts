import { Action } from '@ngrx/store';
import { Message } from '../../../shared/models/message.model';

export const LOAD_MESSAGES = '[EOC/Message] Load Messages';
export const LOAD_MESSAGES_FAIL = '[EOC/Message] Load Messages Fail';
export const LOAD_MESSAGES_SUCCESS = '[EOC/Message] Load Messages Success';

export class LoadMessages implements Action {
	readonly type = LOAD_MESSAGES;
	constructor(public payload: string) {}
}
export class LoadMessagesFail implements Action {
	readonly type = LOAD_MESSAGES_FAIL;
	constructor(payload: any) {}
}

export class LoadMessagesSuccess implements Action {
	readonly type = LOAD_MESSAGES_SUCCESS;
	constructor(public payload: Message[]) {}
}
/////////////
export const CREATE_MESSAGE = '[EOC/Message] Create Message';
export const CREATE_MESSAGE_FAIL = '[EOC/Message] Create Message Fail';
export const CREATE_MESSAGE_SUCCESS = '[EOC/Message] Create Message Success';

export class CreateMessage implements Action {
	readonly type = CREATE_MESSAGE;
	constructor(public payload: Message) {}
}
export class CreateMessageFail implements Action {
	readonly type = CREATE_MESSAGE_FAIL;
	constructor(public payload: any) {}
}
export class CreateMessageSuccess implements Action {
	readonly type = CREATE_MESSAGE_SUCCESS;
	constructor(public payload: Message) {}
}
/////////////
export const UPDATE_MESSAGE = '[EOC/Message] Update Message';
export const UPDATE_MESSAGE_FAIL = '[EOC/Message] Update Message Fail';
export const UPDATE_MESSAGE_SUCCESS = '[EOC/Message] Update Message Success';

export class UpdateMessage implements Action {
	readonly type = UPDATE_MESSAGE;
	constructor(public payload: Message) {}
}
export class UpdateMessageFail implements Action {
	readonly type = UPDATE_MESSAGE_FAIL;
	constructor(public payload: any) {}
}
export class UpdateMessageSuccess implements Action {
	constructor(public payload: Message) {}
	readonly type = UPDATE_MESSAGE_SUCCESS;
}

export const REMOVE_MESSAGE = '[EOC/Message] Remove Message';
export const REMOVE_MESSAGE_FAIL = '[EOC/Message] Remove Message Fail';
export const REMOVE_MESSAGE_SUCCESS = '[EOC/Message] Remove Message Success';

export class RemoveMessage implements Action {
	readonly type = REMOVE_MESSAGE;
}
export class RemoveMessageFail implements Action {
	readonly type = REMOVE_MESSAGE_FAIL;
	constructor(public payload: any) {}
}
export class RemoveMessageSuccess implements Action {
	readonly type = REMOVE_MESSAGE_SUCCESS;
}
/////////////
export const UNLOAD_MESSAGES = '[EOC/Message] Unload Messages';

export class UnLoadMessages implements Action {
	readonly type = UNLOAD_MESSAGES;
}

////////////
export const NAVIGATE_MESSAGES = '[EOC/Message] Open Messages View';

export class NavigateMessages implements Action {
	constructor(public payload: string) {}
	readonly type = NAVIGATE_MESSAGES;
}

export type MessageActions =
	| LoadMessages
	| LoadMessagesFail
	| LoadMessagesSuccess
	| CreateMessage
	| CreateMessageFail
	| CreateMessageSuccess
	| UpdateMessage
	| UpdateMessageFail
	| UpdateMessageSuccess
	| RemoveMessage
	| RemoveMessageFail
	| RemoveMessageSuccess
	| UnLoadMessages
	| NavigateMessages;
