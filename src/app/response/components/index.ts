
import { CallerInfoComponent } from './caller-info/caller-info.component';
import { IncidentDetailsComponent } from './incident-details/incident-details.component';

export const components: any[] = [CallerInfoComponent, IncidentDetailsComponent];

export * from './caller-info/caller-info.component';
export * from './incident-details/incident-details.component';
