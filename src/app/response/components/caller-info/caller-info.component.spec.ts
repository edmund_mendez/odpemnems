import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CallerInfoComponent } from './caller-info.component';

describe('CallerInfoComponent', () => {
  let component: CallerInfoComponent;
  let fixture: ComponentFixture<CallerInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CallerInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CallerInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
