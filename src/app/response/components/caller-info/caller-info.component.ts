import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { parishes } from '@odpemshared/config/data.config';

@Component({
	selector: 'caller-info',
	templateUrl: './caller-info.component.html',
	styleUrls: [ './caller-info.component.css' ]
})
export class CallerInfoComponent implements OnInit {
	@Input() callerForm: FormGroup;
	parishes: string[];

	constructor() {}

	ngOnInit() {
		this.parishes = parishes;
	}
}
