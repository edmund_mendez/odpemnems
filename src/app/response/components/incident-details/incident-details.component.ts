import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
@Component({
	selector: 'incident-details',
	templateUrl: './incident-details.component.html',
	styleUrls: [ './incident-details.component.css' ]
})
export class IncidentDetailsComponent implements OnInit {
	@Input() incidentForm: FormGroup;

	constructor() {}

	ngOnInit() {}
}
