import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { EffectsModule, Effect } from '@ngrx/effects';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import * as fromContainers from './containers';
import * as fromComponents from './components';

import * as fromServices from './services';
import * as fromStore from './store';
import * as fromGuards from './guards';
import { AppMaterialModule } from '../app-material/material.module';

const routes: Routes = [
	{
		path: 'messages',
		children: [
			{
				path: '',
				component: fromContainers.MessagesComponent,
				canActivate: [ fromGuards.MessagesGuard ]
			},
			{
				path: 'new',
				component: fromContainers.MessageComponent
				// canActivate: [ fromGuards.ConfigGuard ]
			},
			{
				path: ':unid',
				component: fromContainers.MessageComponent,
				canActivate: [ fromGuards.MessagesGuard ]
			}
		]
	}
];

@NgModule({
	imports: [
		CommonModule,
		StoreModule.forFeature('eoc', fromStore.reducers),
		EffectsModule.forFeature(fromStore.effects),
		FormsModule,
		ReactiveFormsModule,
		AppMaterialModule,
		RouterModule.forChild(routes)
	],
	declarations: [ ...fromContainers.containers, ...fromComponents.components ],
	exports: [ ...fromComponents.components, ...fromContainers.containers ],
	providers: [ ...fromServices.services, ...fromGuards.guards ]
})
export class ResponseModule {}
