import { Action } from '@ngrx/store';
import { OdpemEvent } from '@odpemshared/models/odpemEvent.model';

export const LOAD_CONFIG_DATA = '[EOC/CONFIG] Load Config';
export const LOAD_CONFIG_DATA_FAIL = '[EOC/CONFIG] Load Config Fail';
export const LOAD_CONFIG_DATA_SUCCESS = '[EOC/CONFIG] Load Config Success';

export class LoadConfigData implements Action {
	readonly type = LOAD_CONFIG_DATA;
}
export class LoadConfigDataFail implements Action {
	readonly type = LOAD_CONFIG_DATA_FAIL;
	constructor(public payload: any) {}
}
export class LoadConfigDataSuccess implements Action {
	readonly type = LOAD_CONFIG_DATA_SUCCESS;
	constructor(public payload: OdpemEvent[]) {}
}

export type ConfigActions = LoadConfigData | LoadConfigDataFail | LoadConfigDataSuccess;
