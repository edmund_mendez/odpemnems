import { Action } from '@ngrx/store';

export const SET_EVENT_ROLE = '[UI State] Switch To New Event, Role ';
export const KEEP_EVENT_ROLE = '[UI State] Keep Current Event, Role ';

export class SetEventRole implements Action {
	readonly type = SET_EVENT_ROLE;
	constructor(public payload: { eventid: string; roleid: string }) {}
}
export class KeepEventRole implements Action {
	readonly type = KEEP_EVENT_ROLE;
}

export type UiActions = SetEventRole | KeepEventRole;
