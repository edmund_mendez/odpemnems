import { NavigationExtras } from '@angular/router';
import { Action } from '@ngrx/store';

export const GO = '[Router] Navigate to location';
export const BACK = '[Router] Navigate Back';
export const FORWARD = '[Router] Navigate Forward';
export const REFRESH = '[Router] Refresh';

export class Go implements Action {
	readonly type = GO;
	constructor(public payload: { path: any[]; query?: object; extras?: NavigationExtras }) {}
}

export class Back implements Action {
	readonly type = BACK;
}
export class Forward implements Action {
	readonly type = FORWARD;
}
export class Refresh implements Action {
	readonly type = REFRESH;
}

export type RouterActions = Go | Back | Forward | Refresh;
