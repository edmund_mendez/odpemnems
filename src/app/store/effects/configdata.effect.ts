import { Injectable, Inject } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import * as fromServices from '../../services';
import * as fromActions from '../actions/config.action';
import { switchMap, map, tap, catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable()
export class ConfigDataEffect {
	constructor(private actions$: Actions, private configDataService: fromServices.ConfigDataService) {}

	@Effect()
	$getConfig = this.actions$
		.ofType(fromActions.LOAD_CONFIG_DATA)
		.pipe(
			switchMap((_) => this.configDataService.getConfigData()),
			map((configData) => new fromActions.LoadConfigDataSuccess(configData)),
			catchError((error) => of(new fromActions.LoadConfigDataFail(error)))
		);
}
