import { Injectable, Inject } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { map } from 'rxjs/operators';
import * as fromStore from '../../store';

@Injectable()
export class UiEffect {
	constructor(private actions$: Actions) {}

	@Effect()
	$setEventRole = this.actions$.ofType(fromStore.SET_EVENT_ROLE).pipe(
		map(
			(action: fromStore.SetEventRole) =>
				new fromStore.Go({
					path: [ '/eoc', action.payload.eventid, action.payload.roleid, 'dashboard' ]
				})
		)
	);
}
