import { Injectable, Inject } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

import { map, tap } from 'rxjs/operators';
import * as fromRouter from '../actions/router.action';

@Injectable()
export class RouterEffect {
	constructor(private actions$: Actions, private router: Router, private location: Location) {}
	@Effect({ dispatch: false })
	$navigate = this.actions$.ofType(fromRouter.GO).pipe(
		map((action: fromRouter.Go) => action.payload),
		tap(({ path, query: queryParams, extras }) => {
			this.router.navigate(path, { queryParams, ...extras });
		})
	);

	@Effect({ dispatch: false })
	navigateBack$ = this.actions$.ofType(fromRouter.BACK).pipe(tap((_) => this.location.back));

	@Effect({ dispatch: false })
	navigateForward$ = this.actions$.ofType(fromRouter.FORWARD).pipe(tap((_) => this.location.forward));

	/* @Effect({ dispatch: true })
	refresh$ = this.actions$.ofType(fromRouter.REFRESH).pipe(
		map((_) => {
			this.router.onSameUrlNavigation = 'reload';
			const path = [ 'eoc', 'eventselect' ];
			return new fromRouter.Go({ path });
		})
	); */
}
