import { RouterEffect } from './router.effect';
import { ConfigDataEffect } from './configdata.effect';
// import {} from './uistate.effect';

export const effects = [ RouterEffect, ConfigDataEffect ];
