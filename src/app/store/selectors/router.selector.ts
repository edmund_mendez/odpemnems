import { createSelector } from '@ngrx/store';
import * as fromRouter from '../reducers';

export const getCurrentEvent = createSelector(
	fromRouter.getRootState,
	(state) => state && state.router.state.params['eventId']
);
export const getCurrentUserRole = createSelector(
	fromRouter.getRootState,
	(state) => state && state.router.state.params['roleId']
);
