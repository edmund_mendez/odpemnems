import { createSelector } from '@ngrx/store';
import * as fromRoot from '../reducers';
import * as fromConfigData from '../reducers/configdata.reducer';
import * as fromRouter from '../selectors/router.selector';

export const getConfigDataState = createSelector(fromRoot.getRootState, (state) => state.config);

export const getConfigLoading = createSelector(getConfigDataState, fromConfigData.getIsLoaded);
export const getConfigLoaded = createSelector(getConfigDataState, fromConfigData.getIsLoaded);
export const getEventEntities = createSelector(getConfigDataState, fromConfigData.getEventEntities);
export const getRoleEntities = createSelector(getConfigDataState, fromConfigData.getRoleEntities);

export const getEvents = createSelector(getEventEntities, (entities) =>
	Object.keys(entities).map((key) => entities[key])
);
export const getRoles = createSelector(getRoleEntities, (entities) =>
	Object.keys(entities).map((key) => entities[key])
);
/* export const getCurrentEventName = createSelector(
	getEventEntities,
	fromRouter.getCurrentEvent,
	(entities, eventid) => entities[eventid] && entities[eventid].EventName
); */
