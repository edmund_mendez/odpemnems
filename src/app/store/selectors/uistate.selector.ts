import { createSelector, createFeatureSelector } from '@ngrx/store';
import * as fromUIState from '../reducers/uistate.reducer';
import * as fromRouter from '../selectors/router.selector';

import { getRootState } from '../reducers';

import { getEventEntities } from '../selectors/configdata.selector';
import * as fromAuth from '../../auth/store/reducers';

export const getUiState = createSelector(getRootState, (state) => state.uiState);

export const getSidebarStatus = createSelector(getUiState, fromUIState.getSidebarStatus);

const getCurrentEvent = createSelector(fromRouter.getCurrentEvent, (eventId) => eventId);
export const getCurrentEventName = createSelector(
	getCurrentEvent,
	getEventEntities,
	(eventId, entities) => entities[eventId] && entities[eventId].EventName
);

export const getCurrentUsername = createSelector(
	fromAuth.getAuthState,
	(state) => state && state.identity.user.username
);
