import { OdpemEvent } from '@odpemshared/models/odpemEvent.model';
import { UserRole } from '@odpemshared/models/role.model';
import * as fromActions from '../actions';
import { roles } from '@odpemshared/config/data.config';

export interface ConfigDataState {
	loading: boolean;
	loaded: boolean;
	eventEntities: { [unid: string]: OdpemEvent };
	roleEntities: { [id: string]: UserRole };
}

const initialData: ConfigDataState = {
	loading: false,
	loaded: false,
	eventEntities: {},
	roleEntities: {}
};

export function reducer(state: ConfigDataState = initialData, action: fromActions.ConfigActions): ConfigDataState {
	switch (action.type) {
		case fromActions.LOAD_CONFIG_DATA: {
			return { ...state, loading: true };
		}
		case fromActions.LOAD_CONFIG_DATA_FAIL: {
			return { ...state, loading: false, loaded: false };
		}
		case fromActions.LOAD_CONFIG_DATA_SUCCESS: {
			const roleEntities = roles.reduce((acc: { [id: string]: UserRole }, role: UserRole) => {
				return { ...acc, [role.id]: role };
			}, {});
			const eventEntities = action.payload.reduce((acc: { [unid: string]: OdpemEvent }, event: OdpemEvent) => {
				return { ...acc, [event.unid]: event };
			}, {});
			return { ...state, roleEntities, eventEntities, loading: false, loaded: true };
		}
	}
	return state;
}

export const getIsLoading = (state: ConfigDataState) => state.loading;
export const getIsLoaded = (state: ConfigDataState) => state.loaded;
export const getEventEntities = (state: ConfigDataState) => state.eventEntities;
export const getRoleEntities = (state: ConfigDataState) => state.roleEntities;
