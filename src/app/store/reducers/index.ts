import { RouterReducerState, routerReducer, RouterStateSerializer } from '@ngrx/router-store';
import { ActionReducerMap, createFeatureSelector, createSelector } from '@ngrx/store';
import { RouterStateSnapshot, ActivatedRouteSnapshot, Params } from '@angular/router';
import * as fromConfig from './configdata.reducer';
import * as fromUiState from './uistate.reducer';

export interface RouterStateURL {
	url: string;
	queryParams: Params;
	params: Params;
}
export interface RootState {
	router: RouterReducerState<RouterStateURL>;
	config: fromConfig.ConfigDataState;
	uiState: fromUiState.UiState;
}
export const reducers: ActionReducerMap<RootState> = {
	router: routerReducer,
	config: fromConfig.reducer,
	uiState: fromUiState.reducer
};

// export const getRouterState = createFeatureSelector<RouterReducerState<RouterStateURL>>('routerReducer');
// export const getRootState = createFeatureSelector<RootState>('root');
export const getRootState = (state: RootState) => state;

export class CustomSerializer implements RouterStateSerializer<RouterStateURL> {
	serialize(routerState: RouterStateSnapshot): RouterStateURL {
		const { url } = routerState;
		const { queryParams } = routerState.root;

		let state: ActivatedRouteSnapshot = routerState.root;
		while (state.firstChild) {
			state = state.firstChild;
		}
		const { params } = state;
		return { url, queryParams, params };
	}
}
