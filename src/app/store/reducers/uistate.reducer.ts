import * as fromActions from '../actions/uistate.action';

export interface UiState {
	sidebarStaus: number;
}

const initialData: UiState = {
	sidebarStaus: 1
};

export function reducer(state: UiState = initialData, action: fromActions.UiActions): UiState {
	/* switch (action.type) {
		case fromActions.SET_EVENT_ROLE: {
			return { ...state, currentEvent: action.payload.eventid, currentUserRole: action.payload.roleid };
		}
	} */
	return state;
}

export const getSidebarStatus = (state: UiState) => state.sidebarStaus;
