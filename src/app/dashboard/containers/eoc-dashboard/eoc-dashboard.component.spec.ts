
import { fakeAsync, ComponentFixture, TestBed } from '@angular/core/testing';

import { EocDashboardComponent } from './eoc-dashboard.component';

describe('EocDashboardComponent', () => {
  let component: EocDashboardComponent;
  let fixture: ComponentFixture<EocDashboardComponent>;

  beforeEach(fakeAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ EocDashboardComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EocDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should compile', () => {
    expect(component).toBeTruthy();
  });
});
