import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { EocDashboardComponent } from './containers/eoc-dashboard/eoc-dashboard.component';
import { AppMaterialModule } from '../app-material/material.module';

const routes: Routes = [
	{
		path: '',
		pathMatch: 'full',
		component: EocDashboardComponent
	}
];
@NgModule({
	imports: [ CommonModule, AppMaterialModule, RouterModule.forChild(routes) ],
	declarations: [ EocDashboardComponent ]
})
export class DashboardModule {}
