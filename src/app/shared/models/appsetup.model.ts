import { UserRole } from './role.model';

export interface AppSetup {
    parishes: string[];
    userRoles: UserRole[];
    messageStatuses: string[];
    messagePriorities: string[];
    messageSources: string[];
}
