export interface Message {
	unid?: string;
	MessageId?: string;
	MessageEventId: string;
	MessageEvent: string;
	MessageDateTime: Date;
	MessageSource: string;
	MessageAuthor: string;
	MessagePrecedence: string;
	MessageStatus: string;
	CallerName: string;
	CallerAddress1: string;
	CallerAddress2: string;
	CallerParish: string;
	CallerPhone1: string;
	CallerEmail: string;
	IncidentDateTime: Date;
	IncidentAddress1: string;
	IncidentAddress2: string;
	IncidentParish: string;
	IncidentDescription: string;
	IncidentSituations: string;
	InjuryCount: number;
	DeathCount: number;
	MissingCount: number;
}
