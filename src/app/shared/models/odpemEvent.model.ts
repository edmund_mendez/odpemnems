export interface OdpemEvent {
    EventName: string;
    EventCode: string;
    EventType: string;
    EventDate: any;
    EventTime: any;
    EventEndDate: any;
    EventEndTime: any;
    EventStatus: string;
    unid: string;
}
