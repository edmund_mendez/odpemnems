import {UserRole, AppSetup} from '../models';

export const roles: UserRole[] = [
    {id: 'CL', name: 'Call Logger'},
    {id: 'MC', name: 'Message Controller'},
    {id: 'OO', name: 'Operations Officer'}
];

export const parishes: string[] = [
    'Kingston',
    'St. Andrew',
    'St. Catherine',
    'Clarendon',
    'Manchester',
    'St. Elizabeth',
    'Westmoreland',
    'Hanover',
    'St. James',
    'Trelawny',
    'St. Ann',
    'St. Mary',
    'Portland',
    'St. Thomas'
];

export const appsetup: AppSetup = {
    userRoles: [
        {id: 'CL', name: 'Call Logger'},
        {id: 'MC', name: 'Message Controller'},
        {id: 'OO', name: 'Operations Officer'}
    ],
    parishes: [
        'Kingston',
        'St. Andrew',
        'St. Catherine',
        'Clarendon',
        'Manchester',
        'St. Elizabeth',
        'Westmoreland',
        'Hanover',
        'St. James',
        'Trelawny',
        'St. Ann',
        'St. Mary',
        'Portland',
        'St. Thomas'
    ],
    messagePriorities: ['0', '1', '2'],
    messageSources: ['Phone', 'Online', 'Walk-in'],
    messageStatuses: ['NEW', 'PROCESSING', 'AWAIT_ACTION', 'COMPLETE', 'DUPLICATE']
};
