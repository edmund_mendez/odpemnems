import { Component, OnInit, Inject } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromAuth from './auth/store';

import { Observable } from 'rxjs/observable';
import { tap } from 'rxjs/operators';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: [ './app.component.css' ]
})
export class AppComponent implements OnInit {
	username$: any;
	constructor(private store: Store<fromAuth.AuthState>) {}
	ngOnInit() {
		this.store.dispatch(new fromAuth.GetIdentity());

		/* this.store.select(fromAuth.getUsername).pipe(
      tap(usr => console.log('app module: ', usr))
     ).subscribe(); */
	}
}
